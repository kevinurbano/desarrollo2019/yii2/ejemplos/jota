﻿DROP DATABASE IF EXISTS jota;
CREATE DATABASE IF NOT EXISTS jota;

USE jota;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  foto varchar(150),
  descripcion text,
  precio float,
  oferta boolean,
  PRIMARY KEY(id)
);

INSERT INTO productos 
  VALUES (DEFAULT, 'Producto 1', '1.png', 
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
           Praesent ut dolor imperdiet, iaculis quam eu, aliquam eros.
           Fusce sed ornare tortor. Curabitur ultrices mauris id libero dapibus fringilla.
           Nulla tristique sollicitudin elit nec lacinia.', 3.50, FALSE),
         (DEFAULT, 'Producto 2', '2.png', 
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
           Sed vel felis porttitor, volutpat nibh ut, convallis orci. 
           Suspendisse feugiat leo a tempus placerat. Aliquam rhoncus tellus ante, 
           nec lobortis augue iaculis blandit.', 6.00, FALSE),
         (DEFAULT, 'Producto 3', '3.png', 
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Curabitur vehicula orci eu erat fermentum pharetra.
           Aenean vel nunc vel mauris convallis condimentum.', 9.00, TRUE),
         (DEFAULT, 'Producto 4', '4.png', 
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Pellentesque ut ligula eros. Donec consequat tortor vitae arcu dignissim varius.
           In orci eros, consequat ut pharetra eget, vehicula in ante.', 1.00, FALSE),
         (DEFAULT, 'Producto 5', '5.png', 
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
           Nunc elit arcu, laoreet vel libero et, pellentesque semper neque.
           Nulla eu nisi sed lectus convallis pellentesque. Duis ut suscipit mi,
           in feugiat arcu.', 7.00, TRUE);